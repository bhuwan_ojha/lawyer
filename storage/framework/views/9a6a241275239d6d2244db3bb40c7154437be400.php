<?php ($pgName = 'Contact Us | Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>Contact Us</h1>
							<span>Latest News & Updates</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="<?php echo e(url('home')); ?>">Home </a></li>
								<li>Contact Us</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="lawyer-fancy-title">
								<h2>Message<span class="lawyer-color"> Us</span></h2>
							</div>
							<div class="lawyer-contact-us-form">
								<form action="<?php echo e(url('contact-submit')); ?>" method="post">
									<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
									<ul>
										<li>
											<input type="text" name="contact_name" placeholder="Name"><i class="fa fa-user"></i>
										</li>
										<li>
											<input type="text" name="contact_email" placeholder="Email"><i class="fa fa-envelope-o"></i>
										</li>
										<li>
											<input type="text" name="contact_phone" placeholder="Contat Number"><i class="fa fa-phone"></i>
										</li>
										<li class="contact-full-form">
											<textarea name="contact_message" placeholder="Message"></textarea>
											<i class="fa fa-comment"></i>
										</li>
										<li><label><input type="submit" value="Send Now"></label></li>
									</ul>
								</form>
							</div>
							<div class="lawyer-fancy-title">
								<h2>Our<span class="lawyer-color"> Map</span></h2>
							</div>
							<div class="lawyer-contact-us-map">
								<div id="map">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4201.073212252619!2d85.3278050978397!3d27.696672743391073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a364bb5935%3A0xf40b4cf2c78cf48a!2sAnamnagar%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1500977063362" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
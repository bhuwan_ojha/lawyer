<?php ($pgName = 'News Title | Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>News</h1>
							<span><?php echo e($data[0]->news_title); ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="index.php">Home </a></li>
								<li>News</li>
								<li><?php echo e($data[0]->news_title); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<figure class="lawyer-figure-thumb"><img src="<?php echo e(url('public/assets/img/about.jpg')); ?>" alt="">
								<figcaption>
									<div class="laywer-thumb-text">
										<h3><?php echo e($data[0]->news_title); ?></h3>
									</div>
								</figcaption>
							</figure>
							<div class="lawyer-rich-editor" style="word-wrap:break-word;">
							<?php echo e(strip_tags($data[0]->news_description)); ?>

							</div>
						</div>
						<aside class="col-md-3">
							<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<header class="lawyer-header-one" id="lawyer-header">
			<div class="lawyer-top-strip lawyer-bgcolor">
				<div class="container">
					<div class="row">
						<aside class="col-md-6">
							<ul class="lawyer-strip-info">
								<li><i class="fa fa-phone"></i> <a href="tel:+977014257991">+977 01 4257991</a></li>
								<li><i class="fa fa-print"></i> <a href="tel:+977014252610">+977 01 4252610</a></li>
							</ul>
						</aside>
						<aside class="col-md-6">
							<div class="lawyer-right-section">
								<ul class="lawyer-strip-info">
									<li><i class="fa fa-envelope-o"></i> <a href="mailto:info@lawyersinnepal.com">info@lawyersinnepal.com</a></li>
								</ul>
							</div>
						</aside>
					</div>
				</div>
			</div>
			<div class="lawyer-mainheader">
				<div class="container">
					<div class="row">
						<aside class="col-md-3">
							<a class="lawyer-logo" href="<?php echo e(url('home')); ?>"><img alt="" src="<?php echo e(url('public/assets/img/logo.png')); ?>"></a>
						</aside>
						<aside class="col-md-9">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button aria-expanded="true" class="navbar-toggle collapsed" data-target="#navbar-collapse-1" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
								</div>
								<div class="collapse navbar-collapse" id="navbar-collapse-1">
									<ul class="nav navbar-nav">
										<li class="active"><a href="<?php echo e(url('home')); ?>">Home</a></li>
										<li>
											<a href="#">About</a>
											<ul class="lawyer-dropdown-menu">
												<li><a href="<?php echo e(url('about')); ?>">The Firm</a></li>
												<li><a href="<?php echo e(url('our-team')); ?>">Our Team</a></li>
												<li><a href="<?php echo e(url('management')); ?>">Management</a></li>
												<li><a href="<?php echo e(url('portfolio')); ?>">Portfolio</a></li>
											</ul>
										</li>
										<li>
											<a href="#">Services</a>
											<ul class="lawyer-dropdown-menu">
												<li><a href="#">Consultation</a>
													<ul class="lawyer-dropdown-menu">
														<li><a href="<?php echo e(url('domestic')); ?>">Domestic</a></li>
														<li><a href="<?php echo e(url('international')); ?>">International</a></li>
													</ul>
												</li>
												<li><a href="<?php echo e(url('banking')); ?>">Banking</a></li>
												<li><a href="<?php echo e(url('contract')); ?>">Contracts</a></li>
											</ul>
										</li>
										<li><a href="<?php echo e(url('gallery')); ?>">Gallery</a></li>
										<li><a href="<?php echo e(url('faq')); ?>">FAQs</a></li>
										<li><a href="<?php echo e(url('news')); ?>">News</a></li>
									</ul>
								</div>
							</nav>
							<a class="lawyer-simple-btn lawyer-bgcolor" href="<?php echo e(url('contact-us')); ?>"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;Contact Us</a>
						</aside>
					</div>
				</div>
			</div>
		</header>
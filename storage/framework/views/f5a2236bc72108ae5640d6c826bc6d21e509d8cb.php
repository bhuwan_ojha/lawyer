<?php ($pgName = 'Gallery| Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>Gallery</h1>
							<span>Picture Gallery</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="<?php echo e(url('home')); ?>">Home </a></li>
								<li>Gallery</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="lawyer-gallery lawyer-modern-gallery">
								<ul class="row">
									<?php if($data!=0): ?>
										<?php foreach($data as $value): ?>
									<li class="col-md-4">
										<div class="lawyer-gallery-wrap">
											<figure>
												<a href="#"><img src="<?php echo e(url('public/uploads/gallery/'.$value->image)); ?>" alt=""></a>
												<figcaption>
													<a href="#"><i class="fa fa-arrows"></i></a> 
													<h6><a href="#"><?php echo e($value->title); ?></a></h6>
												</figcaption>
											</figure>
										</div> 
									</li>
										<?php endforeach; ?>
										<?php endif; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php ($pgName = 'Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-banner">
			<div class="lawyer-banner-one">
				<div class="lawyer-banner-one-layer">
					<img alt="" src="<?php echo e(url('public/assets/img/banner1.jpg')); ?>"> <span class="lawyer-banner-transparent"></span>
					<div class="lawyer-banner-caption">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="lawyer-banner-text-wrap">
										<h2><span>Voice Of</span> Justice Not An Echo</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesq ue congue, arcu eu dapibus varius, nisi quam mollis mi.</p><a class="lawyer-banner-btn lawyer-bgcolor" href="about.php">Learn More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="lawyer-banner-one-layer">
					<img alt="" src="<?php echo e(url('public/assets/img/banner2.jpg')); ?>"> <span class="lawyer-banner-transparent"></span>
					<div class="lawyer-banner-caption">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="lawyer-banner-text-wrap">
										<h2><span>Voice Of</span> Justice Not An Echo</h2>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesq ue congue, arcu eu dapibus varius, nisi quam mollis mi.</p><a class="lawyer-banner-btn lawyer-bgcolor" href="services.php">Learn More</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section lawyer-servicesfull">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="lawyer-fancy-title">
								<h2>Well come To <span class="lawyer-color">Legal Research Associates</span></h2><span>What We Offer <small></small></span>
							</div>
							<div class="lawyer-services lawyer-simple-services">
								<ul class="row">
									<li class="col-md-4">
										<div class="lawyer-services-wrap">
											<i class="icon-auction"></i>
											<h4>Consultation</h4>
											<p>Lorem ipsum dolor sit amet, cs ectetur adipiscing elit.</p>
										</div>
									</li>
									<li class="col-md-4">
										<div class="lawyer-services-wrap">
											<i class="icon-diploma2"></i>
											<h4>Banking</h4>
											<p>Lorem ipsum dolor sit amet, cs ectetur adipiscing elit.</p>
										</div>
									</li>
									<li class="col-md-4">
										<div class="lawyer-services-wrap">
											<i class="icon-money-bag"></i>
											<h4>Finance</h4>
											<p>Lorem ipsum dolor sit amet, cs ectetur adipiscing elit.</p>
										</div>
									</li>
									<li class="col-md-4">
										<div class="lawyer-services-wrap">
											<i class="icon-libra"></i>
											<h4>Contracts</h4>
											<p>Lorem ipsum dolor sit amet, cs ectetur adipiscing elit.</p>
										</div>
									</li>
									<li class="col-md-4">
										<div class="lawyer-services-wrap">
											<i class="icon-money-bag"></i>
											<h4>Joint Venture</h4>
											<p>Lorem ipsum dolor sit amet, cs ectetur adipiscing elit.</p>
										</div>
									</li>
									<li class="col-md-4">
										<div class="lawyer-services-wrap">
											<i class="icon-libra"></i>
											<h4>Taxation</h4>
											<p>Lorem ipsum dolor sit amet, cs ectetur adipiscing elit.</p>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-main-section lawyer-historyfull">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="lawyer-history-wrap">
								<div class="lawyer-fancy-title lawyer-fancy-titleleft">
									<h2>About <span class="lawyer-color">Us</span></h2><span>Who We Are<small></small></span>
								</div>
								<h6><?php echo e(strip_tags(substr($about[0]->page_description,0,219))); ?></p><a class="lawyer-banner-btn lawyer-bgcolor" href="<?php echo e(url('about')); ?>" tabindex="0">Learn More</a>
							</div>
						</div>
						<div class="col-md-6">
							<figure class="lawyer-history-thumb">
								<img alt="" src="<?php echo e(url('public/uploads/pages/'.$about[0]->page_image)); ?>">
							</figure>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-main-section lawyer-latest-newsfull" style="padding-top:30px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="lawyer-fancy-title">
                                <h2>Latest <span class="lawyer-color">News</span></h2>
                                <span>News And Updates<small></small></span>
                            </div>
                            <div class="lawyer-blog lawyer-classic-blog">
                                <ul class="row">
									<?php if($news!=0): ?>
										<?php foreach($news as $value): ?>
                                    <li class="col-md-4">
                                        <figure><a href="<?php echo e(url('news-detail/'.$value->ID)); ?>"><img src="<?php echo e(url('public/uploads/newss/'.$value->news_image)); ?>" alt=""><i class="fa fa-link"></i></a></figure>
                                        <div class="lawyer-classic-blog-text">
                                            <time datetime="2008-02-14"><?php echo e($value->created_date); ?></time>
                                            <h4><a href="<?php echo e(url('news-detail/'.$value->ID)); ?>"><?php echo e($value->news_title); ?></a></h4><span style="word-wrap: break-word;">
                                            <?php echo e(strip_tags(substr($value->news_description,0,116))); ?></span>
											<br>
                                            <a href="<?php echo e(url('news-detail/'.$value->ID)); ?>" class="lawyer-readmore-btn">Read More</a>
                                        </div>
                                    </li>
										<?php endforeach; ?>
									<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
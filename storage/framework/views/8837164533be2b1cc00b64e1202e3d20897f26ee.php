	<script src="<?php echo e(url('public/assets/script/jquery.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/bootstrap.min.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/slick.slider.min.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/fancybox.pack.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/isotope.min.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/progressbar.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/circle-chart.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/functions.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/jPages.js')); ?>" type="text/javascript"></script>
	<script>
        $(function(){
			/* initiate the plugin */
            $("div.holder").jPages({
                containerID  : "itemContainer",
                perPage      : 4,
                startPage    : 1,
                startRange   : 1,
                midRange     : 4,
                endRange     : 1
            });
        });
	</script>
</body>
</html>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body>
<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<section class="body">

	<div class="inner-wrapper">
		<?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Dashboard</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Dashboard</span></li>
							<li><span>Dashboard</span></li>
						</ol>
							<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>

			</section>
		</div>
	</section>
	

	

<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>
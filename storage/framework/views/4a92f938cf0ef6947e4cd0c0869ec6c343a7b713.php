<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php ($add = 1); ?>
<?php if($data!=null): ?>
    <?php ($add = 0); ?>
<?php endif; ?>
<body>
<section class="body">
    <div class="inner-wrapper">
        <?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Manage Team</h2>
                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><span>Manage Team</span></li>
                        <li><span>Business</span></li>
                    </ol>
                    <a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>
            <form method="post" action="<?php echo e(url('/admin/team/saveupdate')); ?>" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <input type="hidden" name="ID" value="<?php echo e(!$add ? $data[0]->ID : ''); ?>">
                <section class="panel">
                    <div class="row">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                </div>
                                <h2 class="panel-title">Basic Information</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" name="member_name" class="form-control"
                                                   value="<?php echo e(!$add ? $data[0]->member_name : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Position</label>
                                            <input type="text" name="member_designation" class="form-control"
                                                   value="<?php echo e(!$add ? $data[0]->member_designation : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Short Description</label>
                                            <textarea name="member_description" class="form-control" value="<?php echo e(!$add ? $data[0]->member_description : ''); ?>"><?php echo e(!$add ? $data[0]->member_description : ''); ?></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <div class="row">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                </div>
                                <h2 class="panel-title">Listing Attributes</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Category</label>
                                            <input type="text" name="member_category" class="form-control"
                                                   value="<?php echo e(!$add ? $data[0]->member_category : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Location</label>
                                            <input type="text" name="member_location"
                                                   value="<?php echo e(!$add ? $data[0]->member_location : ''); ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" name="member_email"
                                                   value="<?php echo e(!$add ? $data[0]->member_email : ''); ?>"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Telephone</label>
                                            <input type="text" name="member_phone"
                                                   value="<?php echo e(!$add ? $data[0]->member_phone : ''); ?>" class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>
                        <h2 class="panel-title">Manage Practice Areas</h2>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Practice Areas</label>
                                    <?php if(!$add): ?>
                                        <?php if($practiceAreas!=0): ?>
                                            <?php foreach($practiceAreas as $value): ?>
                                                <div class="input-group">
                                                    <input type="text" name="practice_area_title[]" value="<?php echo e((!$add) ? $value->practice_area_title : ''); ?>" class="form-control" >
                                                    <br>
                                                    <textarea name="practice_area_description[]" value="<?php echo e((!$add) ? $value->practice_area_description : ''); ?>" class="form-control" ><?php echo e((!$add) ? $value->practice_area_description : ''); ?></textarea>
                                                    <span class="input-group-btn">
														<button class="btn btn-danger removeItenieraries" onclick="deleteThis(<?php echo e($value->ID); ?>)" type="button"><i class="glyphicon glyphicon-trash"></i></button>
        											</span>
                                                </div>

                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <input type="text" name="practice_area_title[]" placeholder="Practice Area Title"  class="form-control" >
                                    <br>
                                        <textarea name="practice_area_description[]" placeholder="Practice Area Description"  class="form-control" ></textarea>
                                    <?php endif; ?>
                                </div>
                                <div class="clonehere"></div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success pull-right" id="clonebtn" onclick="cloneItenieraries()"><i class="glyphicon glyphicon-plus-sign"></i> </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>
                        <h2 class="panel-title">Manage Contents</h2>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Biography</label>
                                    <textarea name="member_biography" class="form-control" id="editor1"><?php echo e(!$add ? $data[0]->member_biography : ''); ?></textarea>
                                    <script>
                                        CKEDITOR.replace('editor1');
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>
                        <h2 class="panel-title">Gallery Image</h2>
                    </header>
                    <div class="panel-body">
                        <?php if(!$add && $data[0]->member_image!=0): ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <img src="<?php echo e(url('public/uploads/team/'.$data[0]->member_image)); ?>" class="proImg" alt="Gallery Image">
                                        <div class="overlay">
                                            <label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
                                            <input type="file"  name="member_image" value="<?php echo e((!$add && $data[0]->member_image!=="") ? $data[0]->member_image : ''); ?>"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
                                        </div>
                                    </div>
                                </div>
                        <?php else: ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="https://www.zinfi.com/wp-content/uploads/2016/11/dummy-img.png" class="proImg" alt="Page Image">
                                    <div class="overlay">
                                        <label for="image-input" title="Upload Page Image"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
                                        <input type="file"  name="member_image" value=""  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </section>
                <div class="addNew">
                    <button class="" type="submit"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </section>
    </div>
</section>

<script src="<?php echo e(url('public/admin-assets/vendor/jquery/jquery.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-cookie/jquery-cookie.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/bootstrap/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/nanoscroller/nanoscroller.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-placeholder/jquery-placeholder.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/select2/js/select2.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/javascripts/theme.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/javascripts/theme.custom.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/javascripts/theme.init.js')); ?>"></script>
<script>
    $('.addNewImage').click(function () {
        $(this).before('<div class="row"><div class="col-sm-3"><img src="http://lorempixel.com/200/200/nature/" class="proImg" alt="Gallery Image"><div class="overlay"><label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label><input type="file" name="travelguide_image[]"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" ></div></div><div class="col-sm-9"><div class="form-group"><label class="control-label">Image Title</label><input type="text" name="image_title[]" class="form-control"></div><div class="form-group"><label class="control-label">Image Description</label><input type="text" name="image_description[]" class="form-control"></div><br><button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr></div></div>');
        $('#whatever').append(structure);
    });

    $(document).on('click', '.removeItenieraries', function () {
        $(this).closest('div').parent('div').remove();
    });

    function deleteThis(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-practice-area')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
                location.reload();

            }
        });

    }

    function cloneItenieraries() {
        $('.clonehere').append('<div class="form-group">'+
            '<label class="control-label">Practice Area</label>'+
            '<div class="input-group">'+
            '<input type="text" name="practice_area_title[]" value="" class="form-control" placeholder="Practice Area Title" >'+
                '<br>'+
            '<textarea  name="practice_area_description[]" value="" class="form-control" placeholder="Practice Area Description" ></textarea>'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-danger removeItenieraries" type="button"><i class="glyphicon glyphicon-trash"></i></button>'+
            '</span>'+
            '</div>'+
            '</div>');
    }
    function deleteImage(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-travel-image')); ?>',
            method: 'post',
            data: {id: id},
            success: function () {
                //location.reload();

            }
        });

    }


</script>
</body>

</html>

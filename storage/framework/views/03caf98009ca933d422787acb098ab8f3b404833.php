<!doctype html>
<html class="fixed">
<head>
    <title><?php echo e(SITE_NAME); ?> - Login</title>
    <meta charset="UTF-8">
    <meta name="keywords" content="HTML5 Admin Template"/>
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap/css/bootstrap.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/font-awesome/css/font-awesome.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/magnific-popup/magnific-popup.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/stylesheets/theme.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/stylesheets/theme-custom.css')); ?>">
    <script src="<?php echo e(url('public/admin-assets/vendor/modernizr/modernizr.js')); ?>"></script>
    <script src="<?php echo e(url('public/admin-assets/vendor/style-switcher/style.switcher.localstorage.js')); ?>"></script>

</head>
<body>
<section class="body-sign">
    <div class="center-sign">
        <a href="#" class="logo pull-left">
            <img src="<?php echo e(url('public/assets/img/logo.png')); ?>" height="54" alt="<?php echo e(SITE_NAME); ?>"/>
        </a>

        <div class="panel panel-sign">
            <div><?php if(session('success')): ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <?php echo e(session('success')); ?>

                        <?php endif; ?></div>
                    <div class="panel-title-sign mt-xl text-right">
                        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Sign
                            In</h2>
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo e(url('admin/authenticate')); ?>" method="post">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-group mb-lg">
                                <label>Username</label>
                                <div class="input-group input-group-icon">
                                    <input name="username" type="text" class="form-control input-lg"/>
                                    <span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
                                </div>
                            </div>

                            <div class="form-group mb-lg">
                                <div class="clearfix">
                                    <label class="pull-left">Password</label>
                                </div>
                                <div class="input-group input-group-icon">
                                    <input name="password" type="password" class="form-control input-lg"/>
                                    <span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="checkbox-custom checkbox-default">
                                        <input id="RememberMe" name="rememberme" type="checkbox"/>
                                        <label for="RememberMe">Remember Me</label>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>

            <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
        </div>
</section>
<!-- end: page -->

<!-- Vendor -->
<script src="<?php echo e(url('public/admin-assets/vendor/jquery/jquery.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-cookie/jquery-cookie.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/style-switcher/style.switcher.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/bootstrap/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/nanoscroller/nanoscroller.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/magnific-popup/jquery.magnific-popup.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-placeholder/jquery-placeholder.js')); ?>"></script>

<!-- Theme Base, Components and Settings -->
<script src="<?php echo e(url('admin-assets/javascripts/theme.js')); ?>"></script>

<!-- Theme Custom -->
<script src="<?php echo e(url('admin-assets/javascripts/theme.custom.js')); ?>"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo e(url('admin-assets/javascripts/theme.init.js')); ?>"></script>
<!-- Analytics to Track Preview Website -->
</body>
</html>
{{--

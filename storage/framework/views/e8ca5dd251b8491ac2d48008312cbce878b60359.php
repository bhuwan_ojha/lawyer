<?php ($pgName = 'Our Portfolio | Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>Our Portfolio</h1>
							<span>Vestibulum at lorem lacinia</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="<?php echo e(url('home')); ?>">Home </a></li>
								<li>About</li>
								<li>Our Portfolio</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-blog lawyer-classic-blog">
								<ul class="row">
									<ul id="itemContainer">
									<?php if($data!=0): ?>
									<?php foreach($data as $value): ?>
									<li class="col-md-6">
										<figure><a href="<?php echo e(url('portfolio-detail/'.$value->ID)); ?>"><img src="<?php echo e(url('public/uploads/portfolios/'.$value->portfolio_image)); ?>" alt=""><i class="fa fa-link"></i></a></figure>
										<div class="lawyer-classic-blog-text">
											<time datetime="2008-02-14 20:00">20 MAY</time>
											<h4><a href="<?php echo e(url('portfolio-detail/'.$value->ID)); ?>">Legal Issues Regarding Paternity</a></h4>
											<ul class="lawyer-classic-blog-option">
												<li><a href="<?php echo e(url('portfolio-detail/'.$value->ID)); ?>"><?php echo e($value->category); ?></a></li>
											</ul>
											<?php echo e(strip_tags(substr($value->portfolio_description,0,116))); ?>

											<br>
											<a href="<?php echo e(url('portfolio-detail/'.$value->ID)); ?>" class="lawyer-readmore-btn">Read More</a>
										</div>
									</li>
										<?php endforeach; ?>
									<?php endif; ?>
									</ul>
								</ul>
							</div>

							<div class="lawyer-pagination">
								<div class="holder" style="text-align: center!important;"></div>
							</div>
						</div>
						<aside class="col-md-3">
							<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>

	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

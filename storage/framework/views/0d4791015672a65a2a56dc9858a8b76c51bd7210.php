<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php ($add = 1); ?>
<?php if(isset($data)!=0): ?>
	<?php ($add = 0); ?>
<?php endif; ?>
<body>
<section class="body">
	<div class="inner-wrapper">
		<?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<section role="main" class="content-body">
			<header class="page-header">
				<h2>Manage News</h2>
				<div class="right-wrapper pull-right">
					<ol class="breadcrumbs">
						<li><a href="index.php"><i class="fa fa-home"></i></a></li>
						<li><span>Manage News</span></li>
						<li><span>Business</span></li>
					</ol>
					<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
				</div>
			</header>
			<form method="post" action="<?php echo e(url('admin/news-save-update')); ?>" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
				<input type="hidden" name="ID" value="<?php echo e(!$add ? $data[0]->ID : ''); ?>" >

				<section class="panel">
					<div class="row">
						<div class="col-md-12">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
								</div>
								<h2 class="panel-title">Listing Attributes</h2>
							</header>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label">News Title</label>
											<input type="text" name="news_title" class="form-control" value="<?php echo e(!$add ? $data[0]->news_title : ''); ?>">
										</div>
									</div>

								</div>

							</div>
						</div>
					</div>
				</section>
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
						</div>
						<h2 class="panel-title">Manage Contents</h2>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">Contents</label>
									<textarea name="news_description" class="form-control" id="editor1">
											<?php echo e(!$add ? $data[0]->news_description : ''); ?>

										</textarea>
									<script>
                                        CKEDITOR.replace( 'editor1' );
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
						</div>
						<h2 class="panel-title">Gallery Image</h2>
					</header>
					<div class="panel-body">
						<?php if(!$add): ?>
							<?php if($data[0]->news_image!==null): ?>
									<div class="row">
										<div class="col-sm-3">
											<img src="<?php echo e(url('public/uploads/newss/'.$data[0]->news_image)); ?>" class="proImg" alt="Gallery Image">
											<div class="overlay">
												<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
												<input type="file"  name="news_image" value="<?php echo e((!$add && $data[0]->news_image!=="") ? $data[0]->news_image : ''); ?>"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
											</div>
										</div>
									</div>
							<?php endif; ?>

						<?php else: ?>
                                <div class="row">
											<div class="col-sm-3">
												<img src="https://www.zinfi.com/wp-content/uploads/2016/11/dummy-img.png" class="proImg" alt="Page Image">
												<div class="overlay">
													<label for="image-input" title="Upload Page Image"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
													<input type="file"  name="news_image" value=""  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
												</div>
											</div>
										</div>
                            <?php endif; ?>
					</div>
				</section>
				<div class="addNew">
					<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</section>
	</div>
</section>

<script src="<?php echo e(url('public/admin-assets/vendor/jquery/jquery.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-cookie/jquery-cookie.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/bootstrap/js/bootstrap.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/nanoscroller/nanoscroller.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/jquery-placeholder/jquery-placeholder.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/vendor/select2/js/select2.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/javascripts/theme.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/javascripts/theme.custom.js')); ?>"></script>
<script src="<?php echo e(url('public/admin-assets/javascripts/theme.init.js')); ?>"></script>
<script>

    $('.image-input').change(function() {
        debugger;
        $(this).val($(this).val().replace("C:\\fakepath\\", ""));
    });



    function deleteImage(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-gallery-image')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
                location.reload();

            }
        });

    }


</script>
<script>
    $(function () {
        $('.new-category').hide();
    })
    $('#category').click(function () {
        if($('#category').val()=='add-new') {
            $('#new-category').removeAttr('disabled');
            $('.new-category').show();
        }else{
            $('#new-category').attr('disabled');
            $('.new-category').hide();
        }
    });
</script>
</body>

</html>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title><?php if(isset($pgName) && is_string($pgName)){echo $pgName;}else{echo 'Legal Research Assocates';} ?></title>
	<link href="<?php echo e(url('public/assets/css/bootstrap.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/font-awesome.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/flaticon.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/slick-slider.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/fancybox.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/style.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/color.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(url('public/assets/css/responsive.css')); ?>" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
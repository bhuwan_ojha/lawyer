<?php ($pgName = 'FAQs | Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>FAQs</h1>
							<span>Frequently Asked Questions</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="<?php echo e(url('home')); ?>">Home </a></li>
								<li>FAQs</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-fancy-title">
								<h2><span class="lawyer-color">FAQs</span></h2>
								<span>Frequently Asked Questions <small></small></span>
							</div>
								<?php if($data!=0): ?>
								<?php foreach($data as $value): ?>
									<div class="panel-group faq-accordion" id="<?php echo e($value->ID); ?>accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading active" role="tab" id="<?php echo e($value->ID); ?>Heading">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#<?php echo e($value->ID); ?>accordion" href="#<?php echo e($value->ID); ?>" aria-expanded="true" aria-controls="collapseOne"><?php echo e($value->question); ?></a>
										</h4>
									</div>
									<div id="<?php echo e($value->ID); ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="<?php echo e($value->ID); ?>Heading">
										<div class="panel-body">
											<?php echo e($value->answer); ?>

										</div>
									</div>
								</div>

							</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						<aside class="col-md-3">
							<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body>
<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<section class="body">

	<div class="inner-wrapper">
		<?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Contact</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Contact</span></li>
							<li><span>Contact</span></li>
						</ol>
							<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<section class="panel">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?php echo e(url('admin-assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf')); ?>">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Message</th>
									<th>Date Added</th>
									<th>Update/Delete</th>
								</tr>
							</thead>
							<tbody>

							<?php foreach($data as $value): ?>
								<tr>
									<td><?php echo e($value->contact_name); ?></td>
									<td><?php echo e($value->contact_email); ?></td>
									<td><?php echo e($value->contact_message); ?></td>
									<td><?php echo e($value->created_date); ?></td>
									<td>
										<div class="btn-group" role="group" aria-label="...">
											<button type="button" class="btn btn-sm btn-danger" onclick="deleteContact(<?php echo e($value->ID); ?>)" title="delete"><i class="fa fa-trash"></i> Delete</button>
										</div>
									</td>
								</tr>
								<?php endforeach; ?>

							</tbody>
						</table>
					</div>
				</section>
			</section>
		</div>
	</section>

<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>
    function deleteContact(id){
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-contact')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
              location.reload();

            }
        })

	}
</script>
</body>
</html>
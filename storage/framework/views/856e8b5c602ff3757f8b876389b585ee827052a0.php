<?php ($pgName = 'John doe | Legal Research Associats'); ?>
<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1><?php echo e($data[0]->member_name); ?></h1>
							<span><?php echo e($data[0]->member_designation); ?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="index.php">Home </a></li>
								<li>Our Team</li>
								<li><?php echo e($data[0]->member_name); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
						<div class="lawyer-attorney-detail">
							<figure><img src="<?php echo e(url('public/uploads/team/'.$data[0]->member_image)); ?>" alt=""></figure>
							<div class="lawyer-attorney-detail-text">
								<h3><?php echo e($data[0]->member_name); ?></h3>
								<ul class="lawyer-attorney-social">
									<li><a href="https://www.facebook.com/" class="icon-facebook2"></a></li>
									<li><a href="https://twitter.com/login" class="icon-social62"></a></li>
									<li><a href="https://pk.linkedin.com/" class="icon-social3"></a></li>
									<li><a href="https://plus.google.com/" class="icon-google-plus2"></a></li>
								</ul>
								<br>
								<span><?php echo e($data[0]->member_category); ?></span>
								<br>
								<?php echo e(strip_tags($data[0]->member_description)); ?>

								<ul class="lawyer-attorney-option">
									<li>
										<h6>Position:</h6>
										<span><?php echo e($data[0]->member_designation); ?></span>
									</li>
									<li>
										<h6>Location:</h6>
										<span><?php echo e($data[0]->member_location); ?></span>
									</li>
									<li>
										<h6>E-Mail:</h6>
										<span><?php echo e($data[0]->member_email); ?></span>
									</li>
									<li>
										<h6>Tel:</h6>
										<span><?php echo e($data[0]->member_phone); ?></span>
									</li>
								</ul>
							</div>
						</div>
						<div class="lawyer-rich-editor">
							<div class="lawyer-section-heading"><h2><?php echo e($data[0]->member_category); ?> Biography</h2></div>
							<?php echo e(strip_tags($data[0]->member_biography)); ?>

						</div>
						<div class="lawyer-section-heading"><h2>Practice Areas</h2></div>
							<div class="row">
								<div class="col-md-12">
									<div class="panel-group lawyer-accordion attorney-accordion" id="accordion" role="tablist" aria-multiselectable="true">
										<?php if($practiceArea!=0): ?>
											<?php foreach($practiceArea as $value): ?>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingOne">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo e($value->ID); ?>" aria-expanded="true" aria-controls="collapseOne"><?php echo e($value->practice_area_title); ?></a>
												</h4>
											</div>
											<div id="<?php echo e($value->ID); ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body">
													<p><?php echo e($value->practice_area_description); ?></p>
												</div>
											</div>
										</div>
											<?php endforeach; ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						<aside class="col-md-3">
							<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
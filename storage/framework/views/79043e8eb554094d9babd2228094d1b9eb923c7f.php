<?php ($pgName = 'Our Team | Legal Research Associates'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>About Our Team</h1>
							<span>Legal Research Associates</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="<?php echo e(url('home')); ?>">Home </a></li>
								<li>About</li>
								<li>Our Team</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-attorney lawyer-attorney-classic">
								<ul class="row">
									<ul id="itemContainer">
									<?php if($data!=0): ?>
										<?php foreach($data as $value): ?>
									<li class="col-md-12">
										<figure><a href="<?php echo e(url('team-detail/'.$value->ID)); ?>"><img src="<?php echo e(url('public/uploads/team/'.$value->member_image)); ?>" alt=""><i class="fa fa-link"></i></a></figure>
										<div class="lawyer-attorney-classic-text">
											<h4><?php echo e($value->member_name); ?></h4>
											<ul class="lawyer-attorney-social">
												<li><a href="https://www.facebook.com/" class="icon-facebook2"></a></li>
												<li><a href="https://twitter.com/login" class="icon-social62"></a></li>
												<li><a href="https://pk.linkedin.com/" class="icon-social3"></a></li>
												<li><a href="https://plus.google.com/" class="icon-google-plus2"></a></li>
											</ul>
											<br>
											<span><?php echo e($value->member_category); ?></span>
											<?php echo e(strip_tags(substr($value->member_description,0,230))); ?>

											<a href="<?php echo e(url('team-detail/'.$value->ID)); ?>" class="lawyer-blog-read-btn">Read More <span></span></a>
										</div>
									</li>
										<?php endforeach; ?>
										<?php endif; ?>
									</ul>
								</ul>
							</div>
							<div class="lawyer-pagination">
								<div class="holder" style="text-align: center!important;"></div>
							</div>
						</div>
						<aside class="col-md-3">
							<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body>
<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<section class="body">

	<div class="inner-wrapper">
		<?php echo $__env->make('admin.includes.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Our Team</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Our Team</span></li>
							<li><span>Our Team</span></li>
						</ol>
						<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<section class="panel">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="">
							<thead>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Date Added</th>
									<th>Update/Delete</th>
								</tr>
							</thead>
							<tbody>

							<?php foreach($data as $value): ?>
								<tr>
									<td><?php echo e($value->member_name); ?></td>
									<td><?php echo e(strip_tags(substr($value->member_description,0,100))); ?></td>
									<td><?php echo e($value->created_date); ?></td>
									<td>
										<div class="btn-group" role="group" aria-label="...">
											<button type="button" class="btn btn-sm btn-warning" onclick="location.href='<?php echo e(url('admin/add-team/'.$value->ID)); ?>'" title="update"><i class="fa fa-cogs"></i> Edit</button>
											<button type="button" class="btn btn-sm btn-danger" onclick="deleteOutTeam(<?php echo e($value->ID); ?>)" title="delete"><i class="fa fa-trash"></i> Delete</button>
										</div>
									</td>
								</tr>
								<?php endforeach; ?>

							</tbody>
						</table>
					</div>
				</section>
			</section>
		</div>
	</section>
	
	<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add New</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						
					
						<div class="col-lg-12">
							<section class="panel">
								<div class="panel-body">
									<form class="form-horizontal form-bordered" method="get">
										<div class="form-group">
											<label class="col-md-3 control-label">Full Name</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Email</label>
											<div class="col-md-6">
												<input type="email" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Phone</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">URL</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label">Category</label>
											<div class="col-md-6">
												<input type="text" class="form-control">
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="addNew">
		<a href="<?php echo e(url('/admin/add-team')); ?>" ><i class="fa fa-plus"></i> Add New</a>
	</div>
<?php echo $__env->make('admin.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>
    function deleteOutTeam(id){
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '<?php echo e(url('admin/delete-team')); ?>',
            method:'post',
            data:{id:id},
            success:function () {
              location.reload();

            }
        })

	}
	

</script>
</body>
</html>
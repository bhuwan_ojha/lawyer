@include('admin.includes.header')

<body>
@include('admin.includes.footer')
<section class="body">

	<div class="inner-wrapper">
		@include('admin.includes.nav')
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Contact</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Contact</span></li>
							<li><span>Contact</span></li>
						</ol>
							<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<section class="panel">
					<div class="panel-body">
						<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="{{url('admin-assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf')}}">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Message</th>
									<th>Date Added</th>
									<th>Update/Delete</th>
								</tr>
							</thead>
							<tbody>

							@foreach ($data as $value)
								<tr>
									<td>{{$value->contact_name}}</td>
									<td>{{$value->contact_email}}</td>
									<td>{{$value->contact_message}}</td>
									<td>{{$value->created_date}}</td>
									<td>
										<div class="btn-group" role="group" aria-label="...">
											<button type="button" class="btn btn-sm btn-danger" onclick="deleteContact({{$value->ID}})" title="delete"><i class="fa fa-trash"></i> Delete</button>
										</div>
									</td>
								</tr>
								@endforeach

							</tbody>
						</table>
					</div>
				</section>
			</section>
		</div>
	</section>

@include('admin.includes.footer')
<script>
    function deleteContact(id){
        var id = id;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{url('admin/delete-contact')}}',
                method: 'post',
                data: {id: id},
                success: function () {
                    location.reload();

                }
            })
        }

	}
</script>
</body>
</html>
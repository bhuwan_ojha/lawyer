@include('admin.includes.header')
@php($add = 1)
@if(($data))
    @php($add = 0)
@endif
<body>
	<section class="body">

		<div class="inner-wrapper">
@include('admin.includes.nav')
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Page</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Page</span></li>
							<li><span>Page</span></li>
						</ol>
						<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="{{url('/admin/page-save-update')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="ID" value="{{!$add ? $data[0]->ID :''}}">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Other Information</h2>
						</header>
						<div class="panel-body">
							<div class="row">										
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Page Title</label>
										<textarea name="page_title" class="form-control">{{(!$add) ? $data[0]->page_title : '' }}</textarea>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Manage Contents</h2>
						</header>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Contents</label>
										<textarea name="page_description" class="form-control" id="editor1">
											{{(!$add) ? $data[0]->page_description : '' }}
										</textarea>
										<script>
											CKEDITOR.replace( 'editor1' );
										</script>
									</div>
								</div>
							</div>
						</div>
					</section>

					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Page Image</h2>
						</header>
						<div class="panel-body">
							@if($add==0)
							<div class="row">
								<div class="col-sm-3">
									<img src="{{url('public/uploads/pages/'.$data[0]->page_image)}}" class="proImg" alt="Gallery Image">
									<div class="overlay">
										<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
										<input type="hidden" name="page_image" value="{{(!$add && $data[0]->page_image!=="") ? $data[0]->page_image : '' }}">
										<input type="file"  name="page_image" value="{{(!$add && $data[0]->page_image!=="") ? $data[0]->page_image : '' }}"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
									</div>
								</div>
							</div>
							@else
								<div class="row">
									<div class="col-sm-3">
										<img src="https://www.zinfi.com/wp-content/uploads/2016/11/dummy-img.png" class="proImg" alt="Page Image">
										<div class="overlay">
											<label for="image-input" title="Upload Page Image"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
											<input type="file"  name="page_image" value=""  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
										</div>
									</div>
								</div>
							@endif
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
		</div>


	</section>
	
	
	@include('admin.includes.footer')
</body>
<script>
    $(document).on('click','.removeAchivement',function() {
        $(this).parent('div').remove();
    });
    $(document).on('click','.removeImage',function() {
        $(this).parent('div').parent('div').remove();
    });

    $('.image-input').change(function() {
        debugger;
        $(this).val($(this).val().replace("C:\\fakepath\\", ""));
    });





    function deleteAchievement(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-achievement')}}',
            method:'post',
            data:{id:id},
            success:function () {
                $(this).parent('div').remove();

            }
        })
    }

    function deleteGalleryImage(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-gallery-image')}}',
            method:'post',
            data:{id:id},
            success:function () {
                $(this).parent('div').remove();

            }
        })
    }

</script>
</html>
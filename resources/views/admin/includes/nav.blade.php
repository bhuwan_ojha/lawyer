<aside class="sidebar-left" id="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            MENU
        </div>
        <div class="sidebar-toggle hidden-xs" data-fire-event="sidebar-left-toggle" data-target="html" data-toggle-class="sidebar-left-collapsed">
            <i aria-label="Toggle sidebar" class="fa fa-bars"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav class="nav-main" id="menu" role="navigation">
                <ul class="nav nav-main">
                   <li class="nav-parent">
                        <a><i aria-hidden="true" class="fa fa-columns"></i> <span>Manage Pages</span></a>
                        <ul class="nav nav-children">
                            <li><a href="{{url('admin/manage-pages')}}">Page List</a></li>
                            <li><a href="{{url('admin/manage-portfolio')}}">Portfolio Manager</a></li>
                            <li><a href="{{url('admin/contact-list')}}">Contact Query</a></li>
                            <li><a href="{{url('admin/team-list')}}">Our Team</a></li>
                            <li><a href="{{url('admin/management-list')}}">Management</a></li>
                            <li><a href="{{url('admin/manage-news')}}">News</a></li>
                            <li><a href="{{url('admin/gallery')}}">Gallery</a></li>
                        </ul>

                    </li>
                    <li class="nav-parent">
                        <a><i aria-hidden="true" class="fa fa-columns"></i> <span>Manage Services</span></a>
                        <ul class="nav nav-children">
                            <li><a href="{{url('admin/manage-domestic')}}">Domestic Services</a></li>
                            <li><a href="{{url('admin/manage-international')}}">International Services</a></li>
                            <li><a href="{{url('admin/manage-banking')}}">Banking Services</a></li>
                            <li><a href="{{url('admin/manage-contract')}}">Contract Services</a></li>
                        </ul>

                    </li>
                    
                </ul>
            </nav>

        </div>
       
    </div>
</aside>
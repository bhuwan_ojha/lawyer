@include('admin.includes.header')
@php($add = 1)
@if(isset($data)!=0)
	@php($add = 0)
@endif
<body>
<section class="body">
	<div class="inner-wrapper">
		@include('admin.includes.nav')
		<section role="main" class="content-body">
			<header class="page-header">
				<h2>Manage Portfolio</h2>
				<div class="right-wrapper pull-right">
					<ol class="breadcrumbs">
						<li><a href="index.php"><i class="fa fa-home"></i></a></li>
						<li><span>Manage Portfolio</span></li>
						<li><span>Business</span></li>
					</ol>
					<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a>
				</div>
			</header>
			<form method="post" action="{{url('admin/portfolio-save-update')}}" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="ID" value="{{ !$add ? $data[0]->ID : '' }}" >
				<section class="panel">
					<div class="row">
						<div class="col-md-12">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
								</div>
								<h2 class="panel-title">Basic Information</h2>
							</header>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-6">
									<div class="form-group">
										<label class="control-label">Category</label>
										<select name="category" class="form-control" id="category">
											<option>Select Category</option>

											@if($listing !==0)
												@foreach($listing as $value)
													<option value="{{$value->category}}" @if(!$add ? $value->category == $data[0]->category : '')selected @endif>{{$value->category}}</option>
												@endforeach
											@endif
											<option value="add-new">Add New</option>
										</select>
									</div>
									<div class="form-group new-category" style="display: none;" >
										<label class="control-label">Add New Category</label>
										<input type="text" name="category" id="new-category" disabled class="form-control">
									</div>

								</div>

							</div>
						</div>
					</div>
				</section>
				<section class="panel">
					<div class="row">
						<div class="col-md-12">
							<header class="panel-heading">
								<div class="panel-actions">
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
								</div>
								<h2 class="panel-title">Listing Attributes</h2>
							</header>
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label">Portfolio Title</label>
											<input type="text" name="portfolio_title" class="form-control" value="{{ !$add ? $data[0]->portfolio_title : '' }}">
										</div>
									</div>

								</div>

							</div>
						</div>
					</div>
				</section>
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
						</div>
						<h2 class="panel-title">Manage Contents</h2>
					</header>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">Contents</label>
									<textarea name="portfolio_description" class="form-control" id="editor1">
											{{ !$add ? $data[0]->portfolio_description : '' }}
										</textarea>
									<script>
                                        CKEDITOR.replace( 'editor1' );
									</script>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
							<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
						</div>
						<h2 class="panel-title">Gallery Image</h2>
					</header>
					<div class="panel-body">
						@if(!$add)
							@if($data[0]->portfolio_image!==null)
									<div class="row">
										<div class="col-sm-3">
											<img src="{{url('public/uploads/portfolios/'.$data[0]->portfolio_image)}}" class="proImg" alt="Gallery Image">
											<div class="overlay">
												<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
												<input type="file"  name="portfolio_image" value="{{(!$add && $data[0]->portfolio_image!=="") ? $data[0]->portfolio_image : '' }}"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
											</div>
										</div>
									</div>
							@endif

						@else
                                <div class="row">
											<div class="col-sm-3">
												<img src="https://www.zinfi.com/wp-content/uploads/2016/11/dummy-img.png" class="proImg" alt="Page Image">
												<div class="overlay">
													<label for="image-input" title="Upload Page Image"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
													<input type="file"  name="portfolio_image" value=""  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
												</div>
											</div>
										</div>
                            @endif
					</div>
				</section>
				<div class="addNew">
					<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
				</div>
			</form>
		</section>
	</div>
</section>

<script src="{{url('public/admin-assets/vendor/jquery/jquery.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/jquery-cookie/jquery-cookie.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/select2/js/select2.js')}}"></script>
<script src="{{url('public/admin-assets/javascripts/theme.js')}}"></script>
<script src="{{url('public/admin-assets/javascripts/theme.custom.js')}}"></script>
<script src="{{url('public/admin-assets/javascripts/theme.init.js')}}"></script>
<script>

    $('.image-input').change(function() {
        debugger;
        $(this).val($(this).val().replace("C:\\fakepath\\", ""));
    });



    function deleteImage(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-gallery-image')}}',
            method:'post',
            data:{id:id},
            success:function () {
                location.reload();

            }
        });

    }


</script>
<script>
    $(function () {
        $('.new-category').hide();
    })
    $('#category').click(function () {
        if($('#category').val()=='add-new') {
            $('#new-category').removeAttr('disabled');
            $('.new-category').show();
        }else{
            $('#new-category').attr('disabled');
            $('.new-category').hide();
        }
    });
</script>
</body>

</html>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model
{
    public function FindAll()
    {
        $data['data'] = DB::table('tbl_team')->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_team')->where('ID', $id)->get();
        return $data;


    }

    public function getPracticeAreas($id)
    {
        $data = DB::table('tbl_practice_area')->where('member_id', $id)->get();
        return $data;
    }

    public function SaveUpdate($data, $practiceAreaData, $id = null)
    {
        if ($id == null) {
            DB::table('tbl_team')->insert($data);
            $id = DB::getPdo()->lastInsertId();

            foreach ($practiceAreaData['practice_area_title'] as $data_key => $data_value) {
                $new_practiceAreaData = array(
                    'practice_area_title' => $practiceAreaData['practice_area_title'][$data_key],
                    'practice_area_description' => $practiceAreaData['practice_area_description'][$data_key],
                    "member_id" => $id
                );
                DB::table('tbl_practice_area')->insert($new_practiceAreaData);
            }

        } else {

            $result = DB::table('tbl_team')->where('ID', $id)->update($data);
            $result = DB::table('tbl_practice_area')->where('member_id', $id)->delete();

            foreach ($practiceAreaData['practice_area_title'] as $data_key => $data_value) {
                $new_practiceAreaData = array(
                    'practice_area_title' => $practiceAreaData['practice_area_title'][$data_key],
                    'practice_area_description' => $practiceAreaData['practice_area_description'][$data_key],
                    "member_id" => $id
                );
                DB::table('tbl_practice_area')->insert($new_practiceAreaData);
            }

        }
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->Login = new Login();
    }

    public function index()
    {
        return view('admin/login');
    }

    public function Authenticate(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $result = $this->Login->LoginAuth($username, $password);
        if ($result) {
            Session::set('user', $username);
            return redirect($this->redirectTo);
        } else {
            return redirect('/admin')->with('error', 'Invalid Credentials');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('/admin');
    }




}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->pages = new Page();
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function ListPages()
    {
        $data = $this->pages->FindAll();
        return view('admin/page-list',['data'=>$data]);
    }


    public function PageForm($id=null)
    {
        if($id!==""){
            $data = $this->pages->getByID($id);
            return view('admin/page-form',['data'=>$data]);
        }else {
            return view('admin/page-form');
        }
    }

    public function SaveUpdate(Request $request)
    {

        if ($request->file('page_image') !== null) {
            $file = $request->file('page_image');
            $destinationPath = public_path() . '/uploads/pages';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'page_title' => $request->input('page_title'),
                'page_description' => $request->input('page_description'),
                'created_date' => getCurrentDate(),
                'page_image' => $filename
            );

        }else{
            $data = array(
                'ID' => $request->input('ID'),
                'page_title' => $request->input('page_title'),
                'page_description' => $request->input('page_description'),
                'created_date' => getCurrentDate(),
            );
        }
        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->pages->SaveUpdate($data, $id);
        } else {
            $this->pages->SaveUpdate($data);
        }

        return redirect('/admin/manage-pages');

    }

    public function UpdatePageStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_page')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeletePage(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_page')->where('ID', $id)->get();
        unlink('uploads/pages/'.$result[0]->page_image);
        $result = DB::table('tbl_page')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-pages');
        }
    }

}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\International;
use Illuminate\Support\Facades\DB;

class InternationalServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->international = new International();
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function ListInternationalServices()
    {
        $data = $this->international->FindAll();
        return view('admin/international-list',['data'=>$data]);
    }


    public function InternationalServiceForm($id=null)
    {
        if($id!==""){
            $data = $this->international->getByID($id);
            return view('admin/international-form',['data'=>$data]);
        }else {
            return view('admin/international-form');
        }
    }

    public function SaveUpdate(Request $request)
    {
        if ($request->file('international_image') !== null) {
            $file = $request->file('international_image');
            $destinationPath = public_path() . '/uploads/international';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'international_title' => $request->input('international_title'),
                'international_description' => $request->input('international_description'),
                'created_date' => getCurrentDate(),
                'international_image' => $filename
            );

        } else {
            $data = array(
                'ID' => $request->input('ID'),
                'international_title' => $request->input('international_title'),
                'international_description' => $request->input('international_description'),
                'created_date' => getCurrentDate(),
            );
        }


        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->international->SaveUpdate($data, $id);
        } else {
            $this->international->SaveUpdate($data);
        }

        return redirect('/admin/manage-international');
    }



    public function UpdateInternationalServiceStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_international')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeleteInternationalService(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_international')->where('ID', $id)->get();
        unlink('uploads/international/'.$result[0]->international_image);
        $result = DB::table('tbl_international')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-international');
        }
    }

}

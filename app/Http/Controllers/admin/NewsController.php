<?php

namespace App\Http\Controllers\admin;

use App\Listing;
use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->news = new News();
        $this->listing = new Listing();
    }

    public function index()
    {
        return view('admin/manage-news');
    }

    public function ListNews()
    {
        $data = $this->news->FindAll();
        return view('admin/news',['data'=>$data]);
    }


    public function NewsForm($id=null)
    {

        if($id !==null){
            $data['data'] = $this->news->getByID($id);
            return view('admin/news-form',$data);
        }else {
            $data['data']= null;
            return view('admin/news-form',$data);
        }
    }

    public function SaveUpdate(Request $request)
    {

        if ($request->file('news_image')!==null) {
            $file = $request->file('news_image');
            $destinationPath = public_path() . '/uploads/newss';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'news_title' => $request->input('news_title'),
                'news_description' => $request->input('news_description'),
                'created_date' => getCurrentDate(),
                'news_image'=>$filename
            );
        }else {
            $data = array(
                'ID' => $request->input('ID'),
                'news_title' => $request->input('news_title'),
                'news_description' => $request->input('news_description'),
                'created_date' => getCurrentDate(),
            );
        }

        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->news->SaveUpdate($data, $id);
        } else {
            $this->news->SaveUpdate($data);
        }

        return redirect('/admin/manage-news');

    }

    public function UpdateNewsStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_news')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeleteNews(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_news')->where('ID', $id)->delete();
        if ($result){
            return  true;
        }
    }


    public static function StaticNews()
    {
        $data = DB::table('tbl_news')->orderby('ID','DESC')->take(3)->get();
        return $data;
    }

    public static function StaticNewsAsc()
    {
        $data = DB::table('tbl_news')->orderby('ID','ASC')->take(2)->get();
        return $data;
    }

}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Domestic;
use Illuminate\Support\Facades\DB;

class DomesticServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->domestic = new Domestic();
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function ListDomesticServices()
    {
        $data = $this->domestic->FindAll();
        return view('admin/domestic-list',['data'=>$data]);
    }


    public function DomesticServiceForm($id=null)
    {
        if($id!==""){
            $data = $this->domestic->getByID($id);
            return view('admin/domestic-form',['data'=>$data]);
        }else {
            return view('admin/domestic-form');
        }
    }

    public function SaveUpdate(Request $request)
    {
        if ($request->file('domestic_image') !== null) {
            $file = $request->file('domestic_image');
            $destinationPath = public_path() . '/uploads/domestic';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'domestic_title' => $request->input('domestic_title'),
                'domestic_description' => $request->input('domestic_description'),
                'created_date' => getCurrentDate(),
                'domestic_image' => $filename
            );

        }else{
            $data = array(
                'ID' => $request->input('ID'),
                'domestic_title' => $request->input('domestic_title'),
                'domestic_description' => $request->input('domestic_description'),
                'created_date' => getCurrentDate(),
            );
        }
        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->domestic->SaveUpdate($data, $id);
        } else {
            $this->domestic->SaveUpdate($data);
        }

        return redirect('/admin/manage-domestic');

    }

    public function UpdateDomesticServiceStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_domestic')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeleteDomesticService(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_domestic')->where('ID', $id)->get();
        unlink('uploads/domestic/'.$result[0]->domestic_image);
        $result = DB::table('tbl_domestic')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-domestic');
        }
    }

}

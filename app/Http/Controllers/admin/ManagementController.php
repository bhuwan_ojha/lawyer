<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Management;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;

class ManagementController extends Controller
{
    private $management;
    public function __construct()
    {
        $this->management = new Management();
    }



    public function ListManagement()
    {
        $data = $this->management->FindAll();
        return view('admin/management-list',$data);
    }

    public function ManagementForm($id=null)
    {
        if($id!==""){
            $data['data'] = $this->management->getByID($id);
            $data['practiceAreas'] = $this->management->getPracticeAreas($id);
            return view('admin/management-form',$data);
        }else {
            $data['data']= null;
            return view('admin/management-form',$data);
        }
    }


    public function SaveUpdate(Request $request)
    {


        if ($request->file('member_image')!==null) {
            $file = $request->file('member_image');
            $destinationPath = public_path() . '/uploads/management';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'member_name' => $request->input('member_name'),
                'member_category' => $request->input('member_category'),
                'member_description' => $request->input('member_description'),
                'member_designation' => $request->input('member_designation'),
                'member_location' => $request->input('member_location'),
                'member_email' => $request->input('member_email'),
                'member_phone' => $request->input('member_phone'),
                'member_biography' => $request->input('member_biography'),
                'created_date' => getCurrentDate(),
                'member_image'=>$filename
            );

        }else {
            $data = array(
                'ID' => $request->input('ID'),
                'member_name' => $request->input('member_name'),
                'member_category' => $request->input('member_category'),
                'member_description' => $request->input('member_description'),
                'member_designation' => $request->input('member_designation'),
                'member_location' => $request->input('member_location'),
                'member_email' => $request->input('member_email'),
                'member_phone' => $request->input('member_phone'),
                'member_biography' => $request->input('member_biography'),
                'created_date' => getCurrentDate(),
            );
        }
        $practiceArea['practice_area_title'] = array();
        $practiceAreaData = array(
            'practice_area_title' => $request->input('practice_area_title'),
            'practice_area_description' => $request->input('practice_area_description'),
        );


        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->management->SaveUpdate($data,$practiceAreaData, $id);
        } else {
            $this->management->SaveUpdate($data,$practiceAreaData);
        }


        return redirect('/admin/management-list');

    }

    public function UpdateManagement($id)
    {
        $data = $this->management->getByID($id);
        return view('admin/management',['data'=>$data]);

    }

    public function DeletePracticeArea(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_practice_area_management')->where('ID', $id)->delete();
        if ($result){
            return redirect()->back()->with('message', 'IT WORKS!');
        }

    }

    public function DeleteManagement(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_management')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/management-list');
        }
    }



}

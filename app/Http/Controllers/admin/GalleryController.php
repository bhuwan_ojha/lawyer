<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;

class GalleryController extends Controller
{
    private $gallery;
    public function __construct()
    {
        $this->gallery = new Gallery();
    }



    public function ListGallery()
    {
        $data = $this->gallery->FindAll();
        return view('admin/gallery',$data);
    }

    public function GalleryForm($id=null)
    {
        if($id!==""){
            $data = $this->gallery->FindAll();
            return view('admin/gallery-form',['data'=>$data]);
        }else {
            return view('admin/gallery-form');
        }
    }


    public function SaveUpdate(Request $request)
    {
        $gallery_image=array();
        foreach ($request->file('image') as $data_key => $data_value) {
            if ($request->file('image')[$data_key]!==null) {
                $file = $request->file('image')[$data_key];
                $destinationPath = public_path() . '/uploads/gallery';
                $filename = time() . '_' . $file->getClientOriginalName();
                $filename = str_replace(' ', '_', $filename);
                $fileName = $file->move($destinationPath, $filename);
                $gallery_image[] = array(
                    'image' =>  $filename,
                    'title' => $request->input('title')[$data_key],
                    'new_name' => $filename,
                    'ID' => $request->input('imageID')
                );
            }
        }
        $this->gallery->SaveUpdate($gallery_image);


        return redirect('/admin/gallery');

    }

    public function UpdateGallery($id)
    {
        $data = $this->gallery->getByID($id);
        return view('admin/gallery',['data'=>$data]);

    }

    public function DeletePracticeArea(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_practice_area')->where('ID', $id)->delete();
        if ($result){
            return redirect()->back()->with('message', 'IT WORKS!');
        }

    }

    public function DeleteGallery(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_gallery')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/gallery-list');
        }
    }




}

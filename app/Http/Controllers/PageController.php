<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function index()
    {
        $data['about'] = DB::table('tbl_page')->where('ID','1')->get();
        $data['news'] = DB::table('tbl_news')->orderby('ID','DESC')->take(3)->get();
        return view('index',$data);
    }


    public function about()
    {
        $data = DB::table('tbl_page')->where('ID','1')->get();
        return view('about',['data'=>$data]);
    }

    public static function StaticAboutUS()
    {
        $data = DB::table('tbl_page')->where('ID','1')->get();
        return view('about',['about'=>$data]);
    }

    public function Portfolio()
    {
        $data = DB::table('tbl_portfolio')->get();
        return view('portfolio',['data'=>$data]);
    }

    public function PortfolioDetail($id)
    {
        $data = DB::table('tbl_portfolio')->where('ID',$id)->get();
        return view('portfolio-detail',['data'=>$data]);
    }

    public function OurTeam()
    {
        $data = DB::table('tbl_team')->get();
        return view('team',['data'=>$data]);
    }

    public function OurTeamDetail($id)
    {
        $data['data'] = DB::table('tbl_team')->where('ID',$id)->get();
        $data['practiceArea'] = DB::table('tbl_practice_area')->where('member_id',$id)->get();
        return view('team-detail',$data);
    }

    public function OurManagement()
    {
        $data = DB::table('tbl_management')->get();
        return view('management',['data'=>$data]);
    }

    public function OurManagementDetail($id)
    {
        $data['data'] = DB::table('tbl_management')->where('ID',$id)->get();
        $data['practiceArea'] = DB::table('tbl_practice_area_management')->where('member_id',$id)->get();
        return view('management-detail',$data);
    }

    public function GetService(Request $request)
    {
        $data['url'] = basename($request->url());
        $data['data'] = DB::table('tbl_'.$data['url'])->get();
        return view('services',$data);
    }

    public function GetServiceDetail(Request $request)
    {
        $data['url'] = ($request->name);
        $id = $request->id;
        $data['data'] = DB::table('tbl_'.$data['url'])->where('ID',$id)->get();
        return view('services-detail',$data);
    }

    public function Faq()
    {
        $data = DB::table('tbl_faq')->get();
        return view('faq',['data'=>$data]);
    }

    public function News()
    {
        $data = DB::table('tbl_news')->get();
        return view('news',['data'=>$data]);
    }

    public function NewsDetail($id)
    {
        $data = DB::table('tbl_news')->where('ID',$id)->get();
        return view('news-detail',['data'=>$data]);
    }

    public function Gallery()
    {
        $data['data'] = DB::table('tbl_gallery')->get();
        return view('gallery',$data);
    }

    public function Contact()
    {
        return view('contact');
    }

    public function ContactSubmit(Request $request)
    {
        $data = array(
            'contact_name' => $request->input('contact_name'),
            'contact_email' => $request->input('contact_email'),
            'contact_phone' => $request->input('contact_phone'),
            'contact_message' => $request->input('contact_message'),
            'created_date' => getCurrentDate(),
        );

        $result = DB::table('tbl_contact')->insert($data);
        if($result){
            return redirect('contact-us');
        }
    }
}

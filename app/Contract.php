<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contract extends Model
{

    public function FindAll()
    {
        $data = DB::table('tbl_contract')->orderby('ID','ASC')->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_contract')->where('ID', $id)->get();
        return $data;
    }



    public function SaveUpdate($data,$id=null)
    {
        if($id==""){

            DB::table('tbl_contract')->insert($data);

        }else{

            DB::table('tbl_contract')->where('ID',$id)->update($data);

        }
    }


}

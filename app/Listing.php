<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Listing extends Model
{

    public function FindAll()
    {
        $data = DB::table('tbl_listing')->orderby('ID','ASC')->get();
        return $data;
    }

    public static function FindAllHeader()
    {
        $data = DB::table('tbl_listing')->orderby('ID','ASC')->get();
        return $data;
    }

    public static function getRegionByCountryName($country)
    {
        $data = DB::table('tbl_listing')->where('country',$country)->orderby('ID','ASC')->get();
        return $data;
    }



    public function FindCategory()
    {
        $data = DB::table('tbl_listing')->orderby('ID','ASC')->select('category')->get('category');
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_listing')->where('ID', $id)->get();
        return $data;
    }

    public function SaveUpdate($data,$id=null)
    {
        if($id==""){
            DB::table('tbl_listing')->insert($data);

        }else{
            DB::table('tbl_listing')->where('ID',$id)->update($data);

        }
    }


}
